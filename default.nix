with (import <nixpkgs> {});
derivation {
  name = "hello";
  builder = "${bash}/bin/bash";
  args = [ "-c" "echo 'Hello World' > $out" ];
  system = builtins.currentSystem;
}
